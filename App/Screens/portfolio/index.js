import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TouchableOpacity,
  FlatList,
  Modal,
} from 'react-native';
import Sleep from '../../Images/sleep.png';
import HomeImage from '../../Images/home.png';
import Arrow from '../../Images/arrow.png';
import Triangle from '../../Images/triangle.png';
import Radio1 from '../../Images/radio1.png';
import Radio2 from '../../Images/radio2.png';

export default class Portfolio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuIndex: 'portfolio',
      portfolioContent: [
        {
          image: Sleep,
          mainHeading: "A cart's version of love",
          painterName: 'Kylie Joger',
          earned: '$250',
          token: 2200,
          price: 2200,
        },
        {
          image: Sleep,
          mainHeading: "A cart's version of love",
          painterName: 'Kylie Joger',
          earned: '$250',
          token: 2200,
          price: 3300,
        },
        {
          image: Sleep,
          mainHeading: "A cart's version of love",
          painterName: 'Kylie Joger',
          earned: '$250',
          token: 2200,
          price: 1100,
        },
        {
          image: Sleep,
          mainHeading: "A cart's version of love",
          painterName: 'Kylie Joger',
          earned: '$250',
          token: 2200,
          price: 2400,
        },
        {
          image: Sleep,
          mainHeading: "A cart's version of love",
          painterName: 'Kylie Joger',
          earned: '$250',
          token: 2200,
          price: 7700,
        },
      ],
      modalVisible: false,
      filterContentData: [
        {
          image1: Radio1,
          image2: Radio2,
          content: 'royalties',
        },
        {
          image1: Radio1,
          image2: Radio2,
          content: 'tokens held',
        },
        {
          image1: Radio1,
          image2: Radio2,
          content: 'zeptacoins',
        },
      ],
      modalIndex:0
    };
  }

  handleFilter = index => {
    this.setState({modalIndex:index})
  };

  render() {
    return (
      <SafeAreaView style={StyleSheet.safeAreaContainr}>
        {/* <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#000" translucent = {true}/> */}
        <View style={{height: '100%', backgroundColor: '#061306'}}>
          <View
            style={{
              alignItems: 'center',
              paddingVertical: 20,
              borderBottomWidth: 2,
              borderBottomColor: '#D4AF37',
              backgroundColor: '#000',
            }}>
            {this.state.menuIndex === 'portfolio' ? (
              <Text
                style={{
                  color: '#D4AF37',
                  textTransform: 'uppercase',
                  fontSize: 20,
                }}>
                portfolio
              </Text>
            ) : this.state.menuIndex === 'home1' ? (
              <Text
                style={{
                  color: '#D4AF37',
                  textTransform: 'uppercase',
                  fontSize: 20,
                }}>
                Home1
              </Text>
            ) : this.state.menuIndex === 'home2' ? (
              <Text
                style={{
                  color: '#D4AF37',
                  textTransform: 'uppercase',
                  fontSize: 20,
                }}>
                Home2
              </Text>
            ) : (
              <Text
                style={{
                  color: '#D4AF37',
                  textTransform: 'uppercase',
                  fontSize: 20,
                }}>
                Home3
              </Text>
            )}
            {this.state.menuIndex === 'portfolio' ? (
              <View style={{position: 'absolute', right: '5%', top: '100%'}}>
                <TouchableOpacity
                  onPress={() => this.setState({modalVisible: true})}
                  style={{padding:"2%"}}
                  >
                  
                  <Image
                    source={Triangle}
                    style={{width: 15, height: 15}}
                    resizeMode="contain"
                    tintColor="#fff"
                  />
                </TouchableOpacity>
              </View>
            ) : null}
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
              presentationStyle="overFullScreen"
              onRequestClose={() => {
                this.setState({modalVisible: false});
              }}>
              <View
                style={{
                  backgroundColor: '#00000099',
                  flex: 1,
                  alignItems: 'flex-end',
                }}>
                <View
                  style={{
                    width: '40%',
                    height: '20%',
                    backgroundColor: '#132639',
                    borderRadius: 5,
                    position: 'absolute',
                    right: 10,
                    top: '6%',
                  }}>
                  <View
                    style={{
                      width: '100%',
                      height: '80%',
                      borderWidth: 1,
                      borderColor: 'skyblue',
                      borderRadius: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.filterContentData.map((data, index) => {
                      let bool = false;
                      let imageColor= "skyblue"
                      if(this.state.modalIndex == index){
                        bool = true
                         imageColor = "#D4AF37"
                      }
                      return (
                        <TouchableOpacity
                          onPress={() => this.handleFilter(index)}>
                          <View
                            style={{
                              flexDirection: 'row',
                              paddingVertical: '2%',
                            }}>
                            <View style={{width: '40%', alignItems: 'center'}}>
                              <Image
                                source={Radio1}
                                style={{width: 15, height: 15}}
                                resizeMode="contain"
                                tintColor={imageColor}
                              />
                            </View>
                            <View
                              style={{width: '60%', alignItems: 'flex-start'}}>
                              <Text
                                style={{
                                  color: bool ? "#D4AF37" : 'skyblue',
                                  textTransform: 'uppercase',
                                  fontSize: 12,
                                }}>
                                {data.content}
                              </Text>
                            </View>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                </View>
              </View>
            </Modal>
          </View>
          <View style={{alignItems: 'center'}}>
            {this.state.menuIndex === 'portfolio' ? (
              <View style={{width: '90%', height: '80%'}}>
                <ScrollView showsVerticalScrollIndicator={false}>
                  <FlatList
                    data={this.state.portfolioContent}
                    renderItem={({item}) => {
                      return (
                        <View
                          style={{
                            width: '100%',
                            flexDirection: 'row',
                            marginVertical: '3%',
                            backgroundColor: '#132639',
                            borderRadius: 5,
                          }}>
                          <View style={{width: '25%'}}>
                            <Image
                              source={item.image}
                              style={{width: '100%', height: 100}}
                              resizeMode="contain"
                            />
                          </View>
                          <View
                            style={{
                              width: '50%',
                              justifyContent: 'center',
                              paddingHorizontal: '2%',
                            }}>
                            <View>
                              <Text style={{color: 'skyblue'}}>
                                {item.mainHeading}
                              </Text>
                              <Text style={{color: 'skyblue', fontSize: 12}}>
                                {item.painterName}
                              </Text>
                            </View>
                            <View
                              style={{
                                flexDirection: 'row',
                                paddingVertical: 3,
                              }}>
                              <Text
                                style={{
                                  color: '#D4AF37',
                                  textTransform: 'uppercase',
                                  fontSize: 12,
                                }}>
                                royalties earned :{' '}
                              </Text>
                              <Text
                                style={{
                                  color: '#D4AF37',
                                  textTransform: 'uppercase',
                                  fontSize: 12,
                                }}>
                                {item.earned}
                              </Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                              <Text
                                style={{
                                  color: '#fff',
                                  textTransform: 'uppercase',
                                  fontSize: 12,
                                }}>
                                {' '}
                                tokens held :{' '}
                              </Text>
                              <Text
                                style={{
                                  color: '#fff',
                                  textTransform: 'uppercase',
                                  fontSize: 12,
                                }}>
                                {item.token}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              width: '25%',
                              alignItems: 'flex-end',
                              justifyContent: 'center',
                            }}>
                            <View style={{padding: '5%'}}>
                              <Image
                                source={Arrow}
                                style={{
                                  width: 15,
                                  height: 15,
                                  transform: [{rotate: '320deg'}],
                                }}
                                resizeMode="contain"
                                tintColor="white"
                              />
                            </View>
                            <TouchableOpacity>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  padding: '6%',
                                  borderRadius: 30,
                                  backgroundColor: '#D4AF37',
                                }}>
                                <Text>ZC :</Text>
                                <Text>{item.price}</Text>
                              </View>
                            </TouchableOpacity>
                          </View>
                        </View>
                      );
                    }}
                    keyExtractor={item => item.id}
                  />
                </ScrollView>
              </View>
            ) : (
              <View style={{alignItems: 'center'}}>
                <Text style={{color: 'white'}}>Home</Text>
              </View>
            )}
          </View>
          {this.state.menuIndex === 'portfolio' ? (
            <View style={{alignItems: 'center'}}>
              <View
                style={{
                  width: '90%',
                  position: 'absolute',
                  bottom: 0,
                  backgroundColor: '#132639',
                  flexDirection: 'row',
                  borderRadius: 6,
                }}>
                <View
                  style={{
                    width: '45%',
                    justifyContent: 'center',
                    paddingLeft: '5%',
                  }}>
                  <Text style={{color: 'skyblue', textTransform: 'uppercase'}}>
                    wallet
                  </Text>
                  <Text style={{color: '#fff'}}>Zeptacoins</Text>
                </View>
                <View
                  style={{
                    width: '55%',
                    alignItems: 'flex-end',
                    justifyContent: 'center',
                  }}>
                  <View style={{padding: '5%'}}>
                    <Image
                      source={Arrow}
                      style={{
                        width: 15,
                        height: 15,
                        transform: [{rotate: '320deg'}],
                      }}
                      resizeMode="contain"
                      tintColor="white"
                    />
                  </View>
                  <TouchableOpacity>
                    <View
                      style={{
                        flexDirection: 'row',
                        padding: '6%',
                        borderRadius: 30,
                        backgroundColor: '#D4AF37',
                        paddingHorizontal: '15%',
                      }}>
                      <Text>3400</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ) : null}
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              flexDirection: 'row',
              width: '100%',
              backgroundColor: '#D4AF37',
              height: '8%',
            }}>
            <TouchableOpacity
              style={
                this.state.menuIndex === 'home1'
                  ? {
                      borderWidth: 1,
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 25,
                      marginVertical: 10,
                      backgroundColor: '#000',
                      paddingHorizontal: 2,
                    }
                  : styles.menuContainer
              }
              onPress={() => this.setState({menuIndex: 'home1'})}>
              <View>
                <Image
                  source={HomeImage}
                  style={styles.HomeImagestyle}
                  resizeMode="contain"
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.menuIndex === 'home2'
                  ? {
                      borderWidth: 1,
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 25,
                      marginVertical: 10,
                      backgroundColor: '#000',
                      paddingHorizontal: 2,
                    }
                  : styles.menuContainer
              }
              onPress={() => this.setState({menuIndex: 'home2'})}>
              <View>
                <Image
                  source={HomeImage}
                  style={styles.HomeImagestyle}
                  resizeMode="contain"
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.menuIndex === 'portfolio'
                  ? {
                      borderWidth: 1,
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 25,
                      marginVertical: 10,
                      backgroundColor: '#000',
                      paddingHorizontal: 2,
                    }
                  : styles.menuContainer
              }
              onPress={() => this.setState({menuIndex: 'portfolio'})}>
              <View>
                <Text style={{textTransform: 'uppercase', color: '#fff'}}>
                  portfolio
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.menuIndex === 'home3'
                  ? {
                      borderWidth: 1,
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 25,
                      marginVertical: 10,
                      backgroundColor: '#000',
                      paddingHorizontal: 2,
                    }
                  : styles.menuContainer
              }
              onPress={() => this.setState({menuIndex: 'home3'})}>
              <View>
                <Image
                  source={HomeImage}
                  style={styles.HomeImagestyle}
                  resizeMode="contain"
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeAreaContainr: {
    flex: 1,
  },
  HomeImagestyle: {
    width: 30,
    height: 30,
  },
  menuContainer: {width: '25%', alignItems: 'center', justifyContent: 'center'},
});
